﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicScript : SingletonMonoBehaviour<MusicScript>
{
    private AudioSource _musicSource;
    
    void Start()
    {
        _musicSource = GetComponent<AudioSource>();
        _musicSource.volume = 0f;

        StartCoroutine(FadeIn());
    }

    private IEnumerator FadeIn()
    {
        float counter = 0;
        while (counter < 5f)
        {
            counter += Time.deltaTime;

            _musicSource.volume = Mathf.Lerp(0f, 0.15f, counter / 5f);

            yield return new WaitForEndOfFrame();
        }
        _musicSource.volume = 0.15f;
    }

    public void FadeOut(float seconds)
    {
        StartCoroutine(FadeOutCo(seconds));
    }

    private IEnumerator FadeOutCo(float seconds)
    {
        float counter = 0;
        while (counter < seconds)
        {
            counter += Time.deltaTime;

            _musicSource.volume = Mathf.Lerp(0.15f, 0f, counter / seconds);

            yield return new WaitForEndOfFrame();
        }
        _musicSource.volume = 0.15f;
    }
}
