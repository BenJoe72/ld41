﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : SingletonMonoBehaviour<GameSettings>
{
    public bool IsInfinite { get; set; }
    public int TurnCount { get; set; }
    public float StartDifficulty { get; set; }
    public float EndDifficulty { get; set; }

    public bool MenuAlreadyLoaded { get; set; }

    public int HighScore { get; set; }
    public int LastScore { get; set; }

    private void Start()
    {
        Object.DontDestroyOnLoad(this.gameObject);
        HighScore = PlayerPrefs.GetInt("HighScore", 0);
    }
}
