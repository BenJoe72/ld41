﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : SingletonMonoBehaviour<PlayerController>
{
    public Transform Player;
    public float MoveTime;
    public AudioSource Audio;

    private Tweener _playerTweener;
    private int _steps;
    private int _nextTurnSteps;

    public bool HasSteps { get { return _steps > 0; } }

    private void Start()
    {
        _playerTweener = Player.GetComponent<Tweener>();
    }
    
    public void AddSteps(int amount)
    {
        _steps += amount;
        UIController.Instance.SetStepText(_steps);
    }

    public void Step(Vector3 position)
    {
        _playerTweener.TweenPosition(Player.position, position, MoveTime);
        _steps--;
        UIController.Instance.SetStepText(_steps);
        Audio.Play();
    }

    public void AddNextTurnSteps(int amount)
    {
        _nextTurnSteps += amount;
    }

    public void NextTurn()
    {
        _steps = _nextTurnSteps;
        _nextTurnSteps = 0;
        UIController.Instance.SetStepText(_steps);
    }
}
