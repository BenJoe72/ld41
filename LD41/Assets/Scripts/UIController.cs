﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIController : SingletonMonoBehaviour<UIController>
{
    public TextMeshProUGUI StepText;
    public TextMeshProUGUI ScoreText;
    public TextMeshProUGUI ComboText;
    public TextMeshProUGUI PerfectText;
    public Transform RenderTexture;

    public Button ThrowButton;
    public Button EndTurnButton;

    public string PerfectString;
    public string FantasticString;

    private string _stepText;
    private string _scoreText;
    private string _comboText;

    private void Start()
    {
        _stepText = StepText.text;
        _scoreText = ScoreText.text;
        _comboText = ComboText.text;

        SetStepText(0);
        SetScoreText(0);
        SetComboText(0);
        SetPerfectText(false, false);

        DisableEndTurnButton();
        DisableThrowButton();
    }

    public void SetStepText(int step)
    {
        StepText.text = string.Format(_stepText, step);
    }

    public void SetScoreText(int point)
    {
        ScoreText.text = string.Format(_scoreText, point);
    }

    public void SetComboText(int point)
    {
        ComboText.text = string.Format(_comboText, point);
    }

    public void SetPerfectText(bool perfect, bool fantastic)
    {
        PerfectText.text = perfect ? (fantastic ? FantasticString : PerfectString) : string.Empty;
    }

    public void EnableThrowButton()
    {
        ThrowButton.interactable = true;
    }

    public void DisableThrowButton()
    {
        ThrowButton.interactable = false;
    }

    public void EnableEndTurnButton()
    {
        EndTurnButton.interactable = true;
    }

    public void DisableEndTurnButton()
    {
        EndTurnButton.interactable = false;
    }
}
