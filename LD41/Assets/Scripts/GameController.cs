﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameController : SingletonMonoBehaviour<GameController>
{
    public Camera MainCamera;
    public Camera GameCamera;

    [Header("Die settings")]
    public Transform[] DiePoint;
    public Transform Die;
    public Transform DieStartPosition;
    public float ThrowForce;
    public float ThrowTorque;

    public int DieNumber { get; set; }
    public bool CanStep { get; private set; }
    public float Difficulty { get; private set; }
    public GamePhase CurrentPhase { get; private set; }

    private Rigidbody _dieRigidBody;
    private bool _plyaerEnabled;
    private bool _playerIsMoving;
    private int _currentTurn;

    public void Start()
    {
        _dieRigidBody = Die.GetComponent<Rigidbody>();
        Difficulty = GameSettings.Instance.StartDifficulty;

        ArrowController.Instance.GenerateFirst();
        CurrentPhase = GamePhase.TurnSetup;

        GameSettings.Instance.MenuAlreadyLoaded = true;

        Blackness.Instance.FadeOut(1f, FirstTurn);
    }

    private void FirstTurn()
    {
        ArrowController.Instance.FirstRow();
        Invoke("NextGamePhase", ArrowController.Instance.RowMoveTime);
        _currentTurn = 1;
    }

    public void ThrowDie()
    {
        UIController.Instance.DisableThrowButton();
        Die.localRotation = Random.rotation;
        Die.position = DieStartPosition.position;
        _dieRigidBody.velocity = Vector3.zero;
        _dieRigidBody.AddForce(DieStartPosition.forward * ThrowForce, ForceMode.Impulse);
        _dieRigidBody.AddTorque(Random.onUnitSphere * ThrowTorque);

        StartCoroutine(CheckDieStop());
    }

    public void NewRowSet()
    {
        NextGamePhase();
    }

    private IEnumerator CheckDieStop()
    {
        yield return new WaitForSeconds(1f);

        while (!_dieRigidBody.IsSleeping())
        {
            yield return new WaitForEndOfFrame();
        }

        GetDieNumber();
    }

    private void GetDieNumber()
    {
        int highest = 0;

        for (int i = 1; i < DiePoint.Length; i++)
        {
            if (DiePoint[i].position.y > DiePoint[highest].position.y)
            {
                highest = i;
            }
        }

        DieNumber = highest + 1;
        PlayerController.Instance.AddSteps(DieNumber);

        NextGamePhase();
    }

    public bool StepTile(Transform tile)
    {
        bool result = false;

        if (!_playerIsMoving && _plyaerEnabled && TileInRange(tile) && PlayerController.Instance.HasSteps)
        {
            PlayerController.Instance.Step(tile.position);
            result = true;
            _playerIsMoving = true;
            Invoke("FinishStep", PlayerController.Instance.MoveTime + 0.1f);
        }

        return result;
    }

    private bool TileInRange(Transform tile)
    {
        float distance = Vector3.Distance(PlayerController.Instance.Player.position, tile.position);
        return distance <= Mathf.Sqrt(2) && distance > 0.1f;
    }

    public void StepPerfect()
    {
        ScoreController.Instance.AddPerfect();
    }

    public void StepCenter()
    {
        ScoreController.Instance.ResetCombo();
    }

    public void StepArrow(RowController.RowSetup arrow)
    {
        ScoreController.Instance.AddCombo();
        ScoreController.Instance.AddStep(arrow);
    }

    public void StepCard()
    {
        CardController.Instance.PullCard();
        DisablePlayer();
    }

    public void FinishCardPull()
    {
        EnablePlayer();
    }

    public void FinishStep()
    {
        _playerIsMoving = false;
    }

    private void NextGamePhase()
    {
        switch (CurrentPhase)
        {
            case GamePhase.TurnSetup:
                CurrentPhase = GamePhase.DiceThrow;
                DiceThrow();
                break;
            case GamePhase.DiceThrow:
                CurrentPhase = GamePhase.PlayerAction;
                FinishCardPull();
                break;
            case GamePhase.PlayerAction:
                CurrentPhase = GamePhase.TurnEvaluation;
                DisablePlayer();
                EvaluateScore();
                break;
            case GamePhase.TurnEvaluation:
            default:
                CurrentPhase = GamePhase.TurnSetup;
                NextTurn();
                break;
        }
    }

    public void EndPlayerAction()
    {
        if (CurrentPhase == GamePhase.PlayerAction)
        {
            NextGamePhase();
        }
    }

    private void NextTurn()
    {
        _currentTurn++;

        if (!GameSettings.Instance.IsInfinite && _currentTurn > GameSettings.Instance.TurnCount)
            EndGame();

        bool endGame = !GameSettings.Instance.IsInfinite && (GameSettings.Instance.TurnCount - _currentTurn) < ArrowController.Instance.Rows.Length - 1;

        Difficulty = Mathf.Lerp(GameSettings.Instance.StartDifficulty, GameSettings.Instance.EndDifficulty, (float)_currentTurn / (float)GameSettings.Instance.TurnCount);

        ScoreController.Instance.ResetPerfect();
        PlayerController.Instance.NextTurn();
        ArrowController.Instance.NextRow(endGame);
        TileController.Instance.ResetTiles();
        ScoreController.Instance.ResetSteps();
    }

    private void EndGame()
    {
        GameSettings.Instance.LastScore = ScoreController.Instance.Score;
        GameSettings.Instance.HighScore = Mathf.Max(ScoreController.Instance.Score, GameSettings.Instance.HighScore);
        BackToMainMenu();
    }

    public void BackToMainMenu()
    {
        Blackness.Instance.FadeIn(1f, SwitchToMainScene);
        MusicScript.Instance.FadeOut(1f);
    }

    public void SwitchToMainScene()
    {
        SceneManager.LoadScene("menu");
    }

    private void EvaluateScore()
    {
        ScoreController.Instance.CalculateTurnScore();
        NextGamePhase();
    }

    private void DiceThrow()
    {
        UIController.Instance.EnableThrowButton();
    }

    private void EnablePlayer()
    {
        UIController.Instance.EnableEndTurnButton();
        _plyaerEnabled = true;
    }

    private void DisablePlayer()
    {
        UIController.Instance.DisableEndTurnButton();
        _plyaerEnabled = false;
    }

    public enum GamePhase
    {
        TurnSetup = 1,
        DiceThrow = 2,
        PlayerAction = 3,
        TurnEvaluation = 4
    }
}
