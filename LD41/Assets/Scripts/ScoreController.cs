﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController : SingletonMonoBehaviour<ScoreController>
{
    public int CorrectStepScore;
    public int IncorrectStepScore;
    public int MissedStepScore;
    public int PerfectMultiplier;
    public int FantasticMultiplier;
    public AnimationCurve ComboCurve;

    public int Score { get; private set; }
    public int Combo { get; private set; }
    public bool Perfect { get; private set; }
    public bool Fantastic { get; private set; }

    public RowController.RowSetup CurrentSteps { get; set; }

    public void AddScore(int points)
    {
        Score += points;
        UIController.Instance.SetScoreText(Score);
    }

    public void AddCombo()
    {
        Combo++;
        UIController.Instance.SetComboText(Combo);
    }

    public void AddPerfect()
    {
        if (!Perfect)
        {
            Perfect = true;
        }
        else
        {
            Fantastic = true;
        }

        UIController.Instance.SetPerfectText(Perfect, Fantastic);
    }

    public void AddStep(RowController.RowSetup step)
    {
        CurrentSteps |= step;
    }

    public void ResetPerfect()
    {
        Perfect = false;
        Fantastic = false;
        UIController.Instance.SetPerfectText(Perfect, Fantastic);
    }

    public void ResetCombo()
    {
        Combo = 0;
        UIController.Instance.SetComboText(Combo);
    }

    public void ResetSteps()
    {
        CurrentSteps = 0;
    }

    public void CalculateTurnScore()
    {
        int turnScore = 0;

        bool up = (ArrowController.Instance.CurrentRow.Setup & RowController.RowSetup.Up) > 0;
        bool down = (ArrowController.Instance.CurrentRow.Setup & RowController.RowSetup.Down) > 0;
        bool left = (ArrowController.Instance.CurrentRow.Setup & RowController.RowSetup.Left) > 0;
        bool right = (ArrowController.Instance.CurrentRow.Setup & RowController.RowSetup.Right) > 0;

        bool up_step = (CurrentSteps & RowController.RowSetup.Up) > 0;
        bool down_step = (CurrentSteps & RowController.RowSetup.Down) > 0;
        bool left_step = (CurrentSteps & RowController.RowSetup.Left) > 0;
        bool right_step = (CurrentSteps & RowController.RowSetup.Right) > 0;

        if (up_step)
            turnScore += up ? CorrectStepScore : IncorrectStepScore;
        else
            turnScore += up ? MissedStepScore : 0;

        if (down_step)
            turnScore += down ? CorrectStepScore : IncorrectStepScore;
        else
            turnScore += down ? MissedStepScore : 0;

        if (left_step)
            turnScore += left ? CorrectStepScore : IncorrectStepScore;
        else
            turnScore += left ? MissedStepScore : 0;

        if (right_step)
            turnScore += right ? CorrectStepScore : IncorrectStepScore;
        else
            turnScore += right ? MissedStepScore : 0;

        if (turnScore > 0)
        {
            if (Fantastic)
                turnScore *= FantasticMultiplier;
            else if (Perfect)
                turnScore *= PerfectMultiplier;

            turnScore = Mathf.CeilToInt(ComboCurve.Evaluate(Combo) * turnScore);
        }

        AddScore(turnScore);
    }
}
