﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class MenuItem : MonoBehaviour
{
    public string TalkativeText;
    public UnityEvent ClickAction;

    private Tweener _tweener;

    private Vector3 offPos;
    private Vector3 onPos;

    private void Start()
    {
        _tweener = GetComponent<Tweener>();

        offPos = transform.position;
        onPos = new Vector3(transform.position.x + 0.27f, transform.position.y, transform.position.z);
    }

    private void OnMouseEnter()
    {
        if (CameraTweenScript.Instance.MenuActive)
        {
            _tweener.TweenPosition(transform.position, onPos, 0.3f);
            CameraTweenScript.Instance.TalkativeText.text = TalkativeText;
            CameraTweenScript.Instance.PlayMenuHover();
        }
    }

    private void OnMouseExit()
    {
        if (CameraTweenScript.Instance.MenuActive)
        {
            _tweener.TweenPosition(transform.position, offPos, 0.3f);
            CameraTweenScript.Instance.TalkativeText.text = string.Empty;
        }
    }

    private void OnMouseDown()
    {
        if (CameraTweenScript.Instance.MenuActive)
        {
            ClickAction.Invoke();
            CameraTweenScript.Instance.PlayMenuClick();
        }
    }

    public void QuickPlay()
    {
        GameSettings.Instance.IsInfinite = false;
        GameSettings.Instance.TurnCount = 10;
        GameSettings.Instance.StartDifficulty = 0f;
        GameSettings.Instance.EndDifficulty = 0.7f;

        Blackness.Instance.FadeIn(1f, GoToGameScene);
    }

    public void AllNight()
    {
        GameSettings.Instance.IsInfinite = true;
        GameSettings.Instance.TurnCount = 20;
        GameSettings.Instance.StartDifficulty = 0f;
        GameSettings.Instance.EndDifficulty = 0.8f;

        Blackness.Instance.FadeIn(1f, GoToGameScene);
    }

    public void Organize()
    {

    }

    public void HowTo()
    {

    }

    public void Quit()
    {
        PlayerPrefs.SetInt("HighScore", GameSettings.Instance.HighScore);
        Blackness.Instance.FadeIn(2f, Application.Quit);
    }

    private void GoToGameScene()
    {
        SceneManager.LoadScene("game");
    }
}
