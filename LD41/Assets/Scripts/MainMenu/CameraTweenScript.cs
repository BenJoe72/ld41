﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CameraTweenScript : SingletonMonoBehaviour<CameraTweenScript>
{
    public Transform CameraFirst;
    public Transform CameraSecond;
    public Transform CameraLast;

    public Transform MonitorBlinder;

    public bool SkipFirstStart;

    public float FirstLength;
    public float SecondLength;
    
    public Light PointLight1;
    public Light PointLight2;
    public Light Spotlight1;
    public Light Spotlight2;

    public AudioSource LightOn;
    public AudioSource Buzz;
    public AudioSource Spotlight1on;
    public AudioSource Spotlight2on;
    public AudioSource Monitor;
    public AudioClip MenuItemHover;
    public AudioClip MenuItemClick;

    public TextMeshPro HighScoreText;
    public TextMeshPro TalkativeText;

    public bool MenuActive { get; private set; }

    private Tweener _tweener;
    
    public void Start()
    {
        if (GameSettings.Instance.MenuAlreadyLoaded || SkipFirstStart)
        {
            Blackness.Instance.FadeOut(1f, LaterStart);
        }
        else
        {
            FirstStart();
        }

        MenuActive = false;
        HighScoreText.text = string.Format(HighScoreText.text, GameSettings.Instance.HighScore, GameSettings.Instance.LastScore);
    }

    public void PlayMenuHover()
    {
        Monitor.clip = MenuItemHover;
        Monitor.Play();
    }

    public void PlayMenuClick()
    {
        Monitor.clip = MenuItemClick;
        Monitor.Play();
    }

    private void LaterStart()
    {
        transform.position = CameraLast.position;

        PointLight1.enabled = true;
        PointLight2.enabled = true;
        Spotlight1.enabled = true;
        Spotlight2.enabled = true;

        MonitorBlinder.gameObject.SetActive(false);
        Monitor.Play();
        MenuActive = true;
    }

    private void FirstStart()
    {
        _tweener = GetComponent<Tweener>();

        transform.position = CameraFirst.position;
        transform.rotation = CameraFirst.rotation;

        PointLight1.enabled = false;
        PointLight2.enabled = false;
        Spotlight1.enabled = false;
        Spotlight2.enabled = false;

        StartCoroutine(Lights());
    }

    private IEnumerator Lights()
    {
        yield return new WaitForSeconds(3f);

        Spotlight1.enabled = true;
        Spotlight1on.Play();

        yield return new WaitForSeconds(2f);

        Spotlight2.enabled = true;
        Spotlight1on.Play();

        yield return new WaitForSeconds(2f);

        StartCoroutine(FlickerOn());
    }

    private IEnumerator FlickerOn()
    {
        float nextWait = 0.8f;
        float nextFlicker = 0.2f;
        for (int i = 0; i < 8; i++)
        {
            PointLight1.enabled = true;
            PointLight2.enabled = true;
            LightOn.Play();
            Buzz.Play();

            nextFlicker -= 0.0125f;

            yield return new WaitForSeconds(nextFlicker);

            PointLight1.enabled = false;
            PointLight2.enabled = false;
            Buzz.Stop();

            nextWait -= UnityEngine.Random.Range(0.1f, 0.5f);

            yield return new WaitForSeconds(nextWait);
        }

        PointLight1.enabled = true;
        PointLight2.enabled = true;
        LightOn.Play();
        Buzz.Play();

        StartCoroutine(CameraMovement());
    }

    private IEnumerator CameraMovement()
    {
        _tweener.TweenTransform(CameraFirst, CameraSecond, FirstLength);
        _tweener.TweenSoundVolume(Buzz, Buzz.volume, 0, FirstLength + SecondLength);

        yield return new WaitForSeconds(FirstLength);

        _tweener.TweenTransform(CameraSecond, CameraLast, SecondLength);

        yield return new WaitForSeconds(SecondLength + 0.75f);

        Buzz.Stop();

        MonitorBlinder.gameObject.SetActive(false);
        Monitor.Play();
        MenuActive = true;
    }
}
