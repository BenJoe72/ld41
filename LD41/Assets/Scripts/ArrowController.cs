﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class ArrowController : SingletonMonoBehaviour<ArrowController>
{
    public RowController[] Rows;
    public Transform[] RowPositions;

    public AnimationCurve DifficultyCurve;

    public float RowMoveTime;

    public RowController CurrentRow { get; private set; }

    private List<RowController> _rowQueue;

    private bool _endGame;

    private void Start()
    {
        _rowQueue = new List<RowController>(Rows);
        CurrentRow = _rowQueue.First();
        _endGame = false;
    }

    public void GenerateFirst()
    {
        foreach (var row in _rowQueue)
        {
            row.SetRow(GenerateSetup());
        }
    }

    public void FirstRow()
    {
        for (int i = 0; i < _rowQueue.Count; i++)
        {
            _rowQueue[i].MoveRow(RowPositions[i+1].position);
        }
    }

    public void NextRow(bool endGame)
    {
        for (int i = 0; i < _rowQueue.Count; i++)
        {
            _rowQueue[i].MoveRow(RowPositions[i].position);
        }

        _endGame = endGame;

        Invoke("SetNewRow", RowMoveTime + 0.5f);
    }

    private void SetNewRow()
    {
        if (!_endGame)
        {
            CurrentRow.SetRowPosition(RowPositions.Last().position);
            CurrentRow.SetRow(GenerateSetup()); 
        }

        _rowQueue.Remove(CurrentRow);

        if (!_endGame)
        {
            _rowQueue.Add(CurrentRow); 
        }

        if (_rowQueue.Any())
        {
            CurrentRow = _rowQueue.First();
        }

        GameController.Instance.NewRowSet();
    }

    private RowController.RowSetup GenerateSetup()
    {
        Array values = Enum.GetValues(typeof(RowController.RowSetup));
        RowController.RowSetup result = (RowController.RowSetup)values.GetValue(Random.Range(0, values.Length));
        
        if (Random.Range(0f, 1f) < GetChance())
            result |= RowController.RowSetup.Down;

        if (Random.Range(0f, 1f) < GetChance())
            result |= RowController.RowSetup.Up;

        if (Random.Range(0f, 1f) < GetChance())
            result |= RowController.RowSetup.Left;

        if (Random.Range(0f, 1f) < GetChance())
            result |= RowController.RowSetup.Right;

        return result;
    }

    private float GetChance()
    {
        return DifficultyCurve.Evaluate(GameController.Instance.Difficulty);
    }
}
