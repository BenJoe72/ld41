﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RowController : MonoBehaviour
{
    public GameObject Left;
    public GameObject Up;
    public GameObject Down;
    public GameObject Right;

    public RowSetup Setup;

    private Tweener _tweener;

    private void Start()
    {
        _tweener = GetComponent<Tweener>();
    }

    public void SetRowPosition(Vector3 pos)
    {
        this.transform.position = pos;
    }

    public void SetRow(RowSetup setup)
    {
        Setup = setup;

        Down.SetActive((setup & RowSetup.Down) != 0);
        Up.SetActive((setup & RowSetup.Up) != 0);
        Left.SetActive((setup & RowSetup.Left) != 0);
        Right.SetActive((setup & RowSetup.Right) != 0);
    }

    public void MoveRow(Vector3 newpos)
    {
        _tweener.TweenPosition(transform.position, newpos, ArrowController.Instance.RowMoveTime);
    }

    [Flags] public enum RowSetup
    {
        Left = 1,
        Up = 2,
        Down = 4,
        Right = 8
    }
}
