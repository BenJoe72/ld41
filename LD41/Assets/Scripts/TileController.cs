﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileController : SingletonMonoBehaviour<TileController>
{
    public Transform[] Tiles;

    public Color OffColor;
    public Color OnColor;

    private void Start()
    {
        ResetTiles();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StepOnTile();
        }
    }

    private void StepOnTile()
    {
        Ray ray = GameController.Instance.MainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, float.MaxValue, LayerMask.GetMask("DanceFloor")))
        {
            if (GameController.Instance.StepTile(hit.transform))
            {
                TileOn(hit.transform);
            }            
        }
    }

    public void ResetTiles()
    {
        foreach (var tile in Tiles)
        {
            TileOff(tile);
        }
    }

    private void TileOn(Transform tile)
    {
        Material mat = tile.GetComponent<Renderer>().material;
        mat.color = OnColor;

        SomeTile some = tile.GetComponent<SomeTile>();
        if (!some.Activated)
        {
            some.Act();
            some.Activated = true;
        }
    }

    private void TileOff(Transform tile)
    {
        Material mat = tile.GetComponent<Renderer>().material;
        mat.color = OffColor;

        SomeTile some = tile.GetComponent<SomeTile>();
        some.Activated = false;
    }
}
