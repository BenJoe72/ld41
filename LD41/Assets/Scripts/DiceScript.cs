﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceScript : MonoBehaviour
{
    public AudioClip[] Clips;

    private AudioSource _source;

    private void Start()
    {
        _source = GetComponent<AudioSource>();
    }

    public void OnCollisionEnter(Collision collision)
    {
        _source.clip = Clips[Random.Range(0, Clips.Length)];
        _source.Play();
    }
}
