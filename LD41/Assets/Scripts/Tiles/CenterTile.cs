﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterTile : SomeTile
{
    public override void Act()
    {
        GameController.Instance.StepCenter();
    }
}
