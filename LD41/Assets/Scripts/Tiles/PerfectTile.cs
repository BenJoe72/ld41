﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerfectTile : SomeTile
{
    public override void Act()
    {
        GameController.Instance.StepPerfect();
    }
}
