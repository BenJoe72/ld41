﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardTile : SomeTile
{
    public override void Act()
    {
        GameController.Instance.StepCard();
    }
}
