﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SomeTile : MonoBehaviour
{
    public bool Activated;
    public abstract void Act();
}
