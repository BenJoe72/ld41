﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowTile : SomeTile
{
    public RowController.RowSetup RowType;

    public override void Act()
    {
        GameController.Instance.StepArrow(RowType);
    }
}
