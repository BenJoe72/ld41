﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Blackness : SingletonMonoBehaviour<Blackness>
{
    public Image Panel;

    private void Start()
    {
        UnityEngine.Object.DontDestroyOnLoad(this.gameObject);
    }

    public void FadeIn(float fadeTime, Action Callback)
    {
        StartCoroutine(Fade(0f, 1f, fadeTime, Callback));
    }

    public void FadeOut(float fadeTime, Action Callback)
    {
        StartCoroutine(Fade(1f, 0f, fadeTime, Callback));
    }

    private IEnumerator Fade(float from, float to, float fadeTime, Action callback)
    {
        float currenttime = 0;

        Color startcolor = new Color(Panel.color.r, Panel.color.g, Panel.color.b, from);
        Color endcolor = new Color(Panel.color.r, Panel.color.g, Panel.color.b, to);

        while (fadeTime >= currenttime)
        {
            currenttime += Time.deltaTime;

            Panel.color = Color.Lerp(startcolor, endcolor, currenttime / fadeTime);
            yield return new WaitForEndOfFrame();
        }

        Panel.color = endcolor;
        callback();
    }
}
