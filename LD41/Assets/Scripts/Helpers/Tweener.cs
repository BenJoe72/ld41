﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Tweener : MonoBehaviour
{
    public void TweenTransform(Transform from, Transform to, float seconds)
    {
        TweenPosition(from.position, to.position, seconds);
        TweenRotation(from.rotation, to.rotation, seconds);
    }

    public void TweenPosition(Vector3 from, Vector3 to, float seconds)
    {
        StartCoroutine(Tween(from, to, seconds));
    }

    public void TweenRotation(Quaternion from, Quaternion to, float seconds)
    {
        StartCoroutine(Tween(from, to, seconds));
    }

    public void TweenSoundVolume(AudioSource source, float from, float to, float seconds)
    {
        StartCoroutine(Tween(source, from, to, seconds));
    }

    private IEnumerator Tween(Vector3 from, Vector3 to, float seconds)
    {
        float elapsed = 0.0f;

        while (elapsed < seconds)
        {
            elapsed += Time.deltaTime;

            this.transform.position = Vector3.Lerp(from, to, elapsed/seconds);

            yield return new WaitForEndOfFrame();
        }

        this.transform.position = to;
    }

    private IEnumerator Tween(Quaternion from, Quaternion to, float seconds)
    {
        float elapsed = 0.0f;

        while (elapsed < seconds)
        {
            elapsed += Time.deltaTime;

            this.transform.rotation = Quaternion.Lerp(from, to, elapsed / seconds);

            yield return new WaitForEndOfFrame();
        }

        this.transform.rotation = to;
    }

    private IEnumerator Tween(AudioSource source, float from, float to, float seconds)
    {
        float elapsed = 0.0f;

        while (elapsed < seconds)
        {
            elapsed += Time.deltaTime;

            source.volume = Mathf.Lerp(from, to, elapsed / seconds);

            yield return new WaitForEndOfFrame();
        }

        source.volume = to;
    }
}