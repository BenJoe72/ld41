﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CardController : SingletonMonoBehaviour<CardController>
{
    public List<Card> cards = new List<Card>
    {
        new Card() { Text = "+1 step to your next turn", Effect = () => { PlayerController.Instance.AddNextTurnSteps(1); } },
        new Card() { Text = "+2 steps to your next turn", Effect = () => { PlayerController.Instance.AddNextTurnSteps(2); } },
        new Card() { Text = "Free PERFECT!", Effect = () => { ScoreController.Instance.AddPerfect(); } },
        new Card() { Text = "Free FANTASTIC!", Effect = () => { ScoreController.Instance.AddPerfect(); ScoreController.Instance.AddPerfect(); } },
        //new Card() { Text = "Double overall score!", Effect = () => { ScoreController.Instance.AddScore(ScoreController.Instance.Score); } },
        new Card() { Text = "+250 points", Effect = () => { ScoreController.Instance.AddScore(250); } },
        new Card() { Text = "+500 points", Effect = () => { ScoreController.Instance.AddScore(500); } },
        new Card() { Text = "+750 points", Effect = () => { ScoreController.Instance.AddScore(750); } },
        new Card() { Text = "+1000 points", Effect = () => { ScoreController.Instance.AddScore(1000); } },
        // TODO: ADD MORE CARDS
    };
    
    public Transform PulledCard;
    public TextMeshPro PulledCardText;
    public Transform PulledCardFirstPos;
    public Transform PulledCardLastPos;
    public Transform PulledCardDownPos;
    public float HoldTime;
    public float PullTime;

    public AudioClip CardPullClip;
    public AudioClip CardDownClip;
    public AudioSource CardAudio;

    private Tweener _pullCardTweener;

    public void Start()
    {
        _pullCardTweener = PulledCard.GetComponent<Tweener>();
    }

    public void PullCard()
    {
        CardAudio.clip = CardPullClip;
        CardAudio.Play();

        Card card = cards[Random.Range(0, cards.Count)];
        PulledCardText.text = card.Text;
        card.Effect();

        _pullCardTweener.TweenTransform(PulledCardFirstPos, PulledCardLastPos, PullTime);
        Invoke("DownCard", PullTime + HoldTime);
    }

    public void DownCard()
    {
        CardAudio.clip = CardDownClip;
        CardAudio.Play();

        _pullCardTweener.TweenTransform(PulledCardLastPos, PulledCardDownPos, PullTime);
        Invoke("ResetPullCard", PullTime + 0.5f);
    }

    public void ResetPullCard()
    {
        PulledCard.position = PulledCardFirstPos.position;
        PulledCard.rotation = PulledCardFirstPos.rotation;
        GameController.Instance.FinishCardPull();
    }
}
